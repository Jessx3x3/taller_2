export default [
    {
        name: "Comida de Gato",
        id: 1,
        description: "Whiska recién nacido",
        price: 9000,
        quantity: 5
    },
    {
        name: "Peluche de Gato",
        id: 2,
        description: "Peluche con forma de ratón",
        price: 2990,
        quantity: 5
    },
    {
        name: "Arena de Gato",
        id: 3,
        description: "Arena con olor a menta",
        price: 3950,
        quantity: 5
    },
    {
        name: "Hueso de Perro",
        id: 4,
        description: "Hueso de dinosaurio",
        price: 15990,
        quantity: 5
    },
    {
        name: "Ropa de Perro",
        id: 5,
        description: "Impermeable para lluvia",
        price: 8990,
        quantity: 5
    },
    {
        name: "Collar Perro o Gato",
        id: 6,
        description: "Color amarillo unisex",
        price: 1950,
        quantity: 5
    }
]